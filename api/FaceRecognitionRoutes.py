# For both Python 2.7 and Python 3.x image encoding from strings
import base64

import face_recognition
from sanic import Blueprint
from sanic.response import json
import requests

import os
from pathlib import Path

face_group = Blueprint(name='face-recognition-apis')
current_working_dir = os.getcwd()


@face_group.get('/')
async def home(request):
    return json(
        {
            "status": True,
            'code': 'ACCESS_DENIED',
            'message': 'api group home, ACCESS_DENIED please contact System Administrator!',
            'results': None
        }
    )


# used to store images info
information_dict = []


# @face_group.get('/demo')
def load_system_images():
    link = 'https://eu-cdn.my-face.app/cdn/storage/media/bulk'
    response = requests.get(link)
    # results array contains many images with their owners.
    data_array = response.json()
    for k in data_array:
        # to create image from binary String
        # Save the Files to tmp Directory in the same path of the Application
        complete_name = os.path.join(current_working_dir + "/tmp/" + k['fileName'])
        # Check if the File is Already Exist, Don't Create it to save memory and time.
        is_file_exist = Path(complete_name)
        information_dict.append({'owner': k['fileOwner'], 'file_name': k['fileName'], 'img_id': k['id']})
        if not is_file_exist.is_file():
            with open(complete_name, "wb") as fh:
                fh.write(base64.b64decode(k['fileData']))
                fh.close()

    return json(True)


results_list = []


@face_group.get("/stream")
async def get_user_stream(request):
    load_system_images()
    print(len(information_dict))
    stream_list = load_images_from_folder(current_working_dir + "/train")
    local_list = load_images_from_folder(current_working_dir + '/tmp')
    print(len(stream_list))
    print(len(local_list))
    found = False

    for stream_img in stream_list:
        # image = face_recognition.load_image_file(current_working_dir + '/tmp/' + local_list[0])
        # local_face = face_recognition.face_encodings(image)[0]
        stream_file = face_recognition.load_image_file(current_working_dir + "/train/" + stream_img)
        stream_face = face_recognition.face_encodings(stream_file)[0]
        match_found = face_recognition.compare_faces([local_list], stream_face)

        if match_found:
            # get distanct
            d = face_recognition.face_distance([local_list], stream_face)
            print(d)
            # get owner information.
            # publish the owner id to users microservice to ge their information.
            owner = None
            """
            {
            "fullName":"Yafatek solutions",
            "phoneNumber":"009053488541",
            "country":"Turkey",
            "age":30
            }
            """
            results_list.append({
                'fullName': 'yafatek',
                'match': '45%',
                'country': 'Turkey',
                'age': 30,
                "imgUrl": "https://eu-cdn.my-face.app/cdn/storage/media/load?origin=" + "3a035b02-d3a1-4f9f-9968-2b3abb0b930c"
            })
            found = True

    return json(found)


# @face_group.route("/stream", methods=['POST'])
# def post_json(request):
#     test_file = request.files.get('file')
#
#     file_parameters = {
#         'body': test_file.body,
#         'name': test_file.name,
#         'type': test_file.type,
#     }
#
#     return json({"received": True, "file_names": request.files.keys(), "test_file_parameters": file_parameters})
# def post_json(request):
#     if not os.path.exists(config["upload"]):
#         os.makedirs(config["upload"])
#     test_file = request.files.get('file')
#     file_parameters = {
#         'body': test_file.body,
#         'name': test_file.name,
#         'type': test_file.type,
#     }
#     if file_parameters['name'].split('.')[-1] == 'pdf':
#         file_path = f"{config['upload']}/{str(datetime.now())}.pdf"
#         with open(file_path, 'wb') as f:
#             f.write(file_parameters['body'])
#         f.close()
#         print('file wrote to disk')
#
#         return json({"received": True, "file_names": request.files.keys(), "success": True})
#     else:
#         return json({"received": False, "file_names": request.files.keys(), "success": False, "status": "invalid file uploaded"})


# Compare faces
# def dis():
#     pe = []
#     for i in range(len(im)):
#         # results[i] = face_recognition.compare_faces([im[i]], unknown_face_encoding)
#         face_dis = face_recognition.face_distance([im[i]], unknown_face_encoding)
#         ff = float(1 - face_dis)
#
#         if ff >= 0.30:
#             percen = "{:.0%}".format(ff)
#             pe.append(percen)
#
#     return pe

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        # img = cv2.imread(os.path.join(folder, filename))
        # img = face_recognition.face_encodings(img)[0]
        # if img is not None:
        images.append(filename)

    return images
