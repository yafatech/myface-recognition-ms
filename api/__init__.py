from sanic import Blueprint

from api.FaceRecognitionRoutes import face_group

api_group = Blueprint.group(face_group, url_prefix='/api/v1/recognition')
