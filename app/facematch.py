import os

import cv2
import face_recognition


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))
        img = face_recognition.face_encodings(img)[0]
        if img is not None:
            images.append(img)

    return images


im = load_images_from_folder('./App/train')

print(len(im))

# image_of_bill = face_recognition.load_image_file('./img/known/img1.jpg')
# print(image_of_bill)
# bill_face_encoding = face_recognition.face_encodings(image_of_bill)[0]
#
unknown_image = face_recognition.load_image_file(
    './App/test/obama1.jpg')
# face_locations = face_recognition.face_locations(unknown_image)

unknown_face_encoding = face_recognition.face_encodings(unknown_image)[0]


# Compare faces
def dis():
    pe = []
    for i in range(len(im)):

        # results[i] = face_recognition.compare_faces([im[i]], unknown_face_encoding)
        face_dis = face_recognition.face_distance([im[i]], unknown_face_encoding)
        ff = float(1 - face_dis)

        if ff >= 0.30:
            percen = "{:.0%}".format(ff)
            pe.append(percen)

    return pe


print(dis())
