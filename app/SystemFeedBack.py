from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Pic(Base):
    __tablename__ = 'pic'
    idpic = Column('idpic', Integer, primary_key=True)
    caption = Column('caption', VARCHAR(45))
    img = Column('img', BLOB)

# q=session.query(Pic).all()
