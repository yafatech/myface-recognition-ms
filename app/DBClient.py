from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
engine = create_engine('sqlite:///:memory:', echo=True)
# engine = create_engine('mysql://root:roro@locl host/test')

Session = sessionmaker(bind=engine)
# session object.
session = Session()
