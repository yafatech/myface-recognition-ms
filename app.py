from sanic import Sanic
from sanic.response import json

from api import api_group

app = Sanic(name='my-face-recognition-ms')

# face api group blue prints.
app.blueprint(api_group)


@app.get('/')
async def index(request):
    return json({'status': True, 'message': 'Success!'})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4000)
