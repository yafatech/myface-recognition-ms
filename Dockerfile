FROM python:3.8
COPY requirements.txt /app/

# install gcc
#  RUN apt-get update -y \
#  && apt-get dist-upgrade -y \
#  && apt-get install build-essential -y \
#  && apt-get install manpages-dev -y \
#  && apt-get install cmake -y \
#  && apt-get clean

# install cmake
RUN wget -qO- "https://cmake.org/files/v3.12/cmake-3.12.1-Linux-x86_64.tar.gz" | \
  tar --strip-components=1 -xz -C /usr/local


# install dlib
RUN git clone https://github.com/davisking/dlib.git
#WORKDIR /dlib
#RUN mkdir build
#WORKDIR /dlib/build
#RUN cmake ..
#RUN cmake --build .
WORKDIR /dlib
RUN python3 setup.py install

RUN pip3 install face_recognition

#RUN pip3 install opencv-python


# install dlib
#RUN cd ~ && \
#    mkdir -p dlib && \
#    git clone -b 'v19.9' --single-branch https://github.com/davisking/dlib.git dlib/ && \
#    cd  dlib/ && \
#    python3 setup.py install --yes USE_AVX_INSTRUCTIONS

#RUN pip3 install dlib
#RUN python3 -m pip3 install  https://files.pythonhosted.org/packages/1e/62/aacb236d21fbd08148b1d517d58a9d80ea31bdcd386d26f21f8b23b1eb28/dlib-19.18.0.tar.gWORKDIR /dlibz
#WORKDIR /dlib
#RUN python3 setup.py install


WORKDIR /app
RUN pip install --upgrade pip \
    &&  pip install --trusted-host pypi.python.org --requirement requirements.txt
COPY app.py /app
COPY main.py /app

CMD ["python3", "app.py"]
