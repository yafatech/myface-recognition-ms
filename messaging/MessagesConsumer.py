# create connection Object.
import pika as pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

# listen to channel
# and Connect to the channel
channel = connection.channel()

# make sure the queue is exist.
channel.queue_declare(queue='hello')


# receive the Message by Callback method.
def callback(ch, method, properties, body):
    print("[x] Received %r" % body)


# tell RabbitMQ that this particular callback function should receive messages from our hello queue
channel.basic_consume(
    queue='hello',
    auto_ack=True,
    # pass the message to the callback method.
    on_message_callback=callback)

# infinity loop to keep listening.
print(" [*] waiting for message. To Exit press CTRL+C")
channel.start_consuming()
